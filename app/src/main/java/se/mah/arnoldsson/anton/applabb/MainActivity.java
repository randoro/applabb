package se.mah.arnoldsson.anton.applabb;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView tvName;
    private TextView tvPhone;
    private TextView tvEmail;
    private TextView tvSummary;
    private EditText etName;
    private EditText etPhone;
    private EditText etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeComponents();
        registerListeners();
    }

    private void registerListeners() {
        Button bnSummary = (Button)findViewById(R.id.bnSummary);
        bnSummary.setOnClickListener(new ButtonListener());
        Button bnColor = (Button)findViewById(R.id.bnColor);
        bnColor.setOnClickListener(new ChangeColor());
    }

    private void initializeComponents() {
        tvName = (TextView)findViewById(R.id.tvName);
        tvPhone = (TextView)findViewById(R.id.tvPhone);
        tvEmail = (TextView)findViewById(R.id.tvEmail);
        tvSummary = (TextView)findViewById(R.id.tvSummary);
        etName = (EditText)findViewById(R.id.etName);
        etPhone = (EditText)findViewById(R.id.etPhone);
        etEmail = (EditText)findViewById(R.id.etEmail);
    }

    private class ButtonListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String str = summary();
            tvSummary.setText(str);
        }

        private String summary() {
            String res;
            res = "Name="+etName.getText().toString() +
                    ", phone="+etPhone.getText().toString()+
                    ", email=" +etEmail.getText().toString();
            return res;
        }
    }

    private class ChangeColor implements View.OnClickListener {
        private boolean redColor = true;

        @Override
        public void onClick(View v) {
            if(redColor) {
                tvName.setTextColor(0xFFFF6600);
                tvPhone.setTextColor(0xFFFF6600);
                tvEmail.setTextColor(0xFFFF6600);
            }
            else {
                tvName.setTextColor(Color.RED);
                tvPhone.setTextColor(Color.RED);
                tvEmail.setTextColor(Color.RED);
            }

            redColor = !redColor;

        }
    }
}
